******************************************************************** 
                     D R U P A L    M O D U L E
********************************************************************
Name: teaser_images.module 
Author: Robert Castelo <services at cortextcommunications dot com> 
Drupal: 5.x
********************************************************************
DESCRIPTION:

Automatically converts images in teasers (home page summaries) to
thumbnails (small version of the original images).

Note:
Images must be in the Drupal file system - when an image is added to a site
through Drupal, a thumbnail is automatically created, this is the
thumbnail that this module uses. Images linked to from
other sites will not be resized.

Requires image.module to be installed and enabled


********************************************************************
INSTALLATION:

Note: 
It is assumed that you have Drupal up and running.  Be sure to
check the Drupal web site if you need assistance.  If you run into
problems, you should always read the INSTALL.txt that comes with the
Drupal package and read the online documentation.

Image module must be insalled and enabled first.


1. Place the entire publication directory into your Drupal modules/
directory.

3. Enable the teaser_images module by navigating to:

Administer > Site Building > Modules

Click the 'Save configuration' button at the bottom to commit your
changes.



********************************************************************
CONFIGURATION:

1. Create a teaser image size at:
    
    Administer > Site Configuration > Image
    
    Label must be 'teaser'
    
    
********************************************************************
LIMITATIONS

* Images must be in the directory: files/images/
* Images inserted as 'filtered' (instead of 'HTML') won't be resized


********************************************************************
AUTHOR CONTACT

- Report Bugs/Request Features:
  http://drupal.org/project/teaser_images
   
- Comission New Features:
   http://drupal.org/user/3555/contact
   
- Want To Say Thank You:
   http://www.amazon.com/gp/registry/O6JKRQEQ774F

        
********************************************************************
ACKNOWLEDGEMENT

- Drupal 5 update patch by heimstein.

